# KANO

![kano img](https://upload.wikimedia.org/wikipedia/en/8/81/KanoMKXrender.png)

## Project to build image and deploy to ECS from a specific branch

1. Get source
2. Build image
3. Create task definition
4. Create/update service
5. Notify

## How to test kano

To test a new version you have two different ways.

### First way to run

1. Modify the code
2. Run `docker build . -t $ECR/kano-test`
3. Run something like this: `docker run -v /var/run/docker.sock:/var/run/docker.sock -v ~/.aws:/root/.aws -e SERVICE_NAME=getninjas-all -e REPO=getninjas -e BRANCH=master -w /kano -e ENV=goro -e NOOB_SAIBOT_KEY=${NOOBT_SAIBOT_KEY} -e USER=harry kano-test`

PS: You can get the key from noobsaibot on parameter store noobsaibot/production/X_API_KEY

### Second way to run

1. Modify the code
2. Run `docker build . -t $ECR/kano-test`
3. Run `docker push $ECR/kano-test`
4. Modify the file `task-definition-template-kano.json` to something like this:

``` json
{
  "taskDefinition": "kano-homolog-deploy",
  "overrides": {
    "containerOverrides": [{
      "name": "kano-homolog-deploy",
      "environment": [
        { "name": "SERVICE_NAME",  "value": "getninjas-all" },
        { "name": "REPO",          "value": "getninjas" },
        { "name": "BRANCH",        "value": "master" },
        { "name": "ENV",           "value": "goro" },
        { "name": "USER",          "value": "wbotelhos" }
      ]
    }]
  }
}
```

5. And finally run `aws ecs run-task --cluster nexus --cli-input-json file://${PATH_TO_KANO}/kano/templates/task-definition-template-kano.json`

After this, kano will talk to you on deploy channel and log everything on `kano_test_deploy_${DATE.NOW}.log`

___

Don't forget to merge your branch into homolog branch. Doing this, droneCI will check your code
