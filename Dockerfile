FROM docker:stable-dind

WORKDIR /kano

ENV GOPATH /root/go
ENV SENTRY_DSN https://735e311f5b344423b142363e43342eae:e6e97b0b876a45f7ac99eb1f54da3674@sentry.io/1528775

ADD ./requirements.txt        /requirements.txt

RUN apk update && apk --update add --no-cache bash perl curl jq git groff less python3 go \
    python3-dev libffi-dev libressl-dev musl-dev make gcc \
    mariadb-client zip unzip \
    && curl -sL https://sentry.io/get-cli/ | bash \
    && pip3 install -r /requirements.txt \
    && go get github.com/kubernetes/kompose \
    && cd $GOPATH/src/github.com/kubernetes/kompose \
    && git remote add forked https://github.com/getninjas/kompose.git \
    && git pull forked master \
    && go build -o kompose main.go \
    && cp ./kompose /usr/local/bin/ \
    && cd - \
    && curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl \
    && apk --purge -v del python3-dev musl-dev gcc

ADD ./scripts/entrypoint.sh   /entrypoint.sh
ADD ./scripts/common.sh       /common.sh
ADD ./scripts/deploy.sh       /usr/local/bin/deploy
ADD ./scripts/runner.sh       /usr/local/bin/runner
ADD ./scripts/slackpost.sh    /usr/local/bin/slackpost
ADD ./scripts/custom.sh       /custom.sh
ADD ./scripts/deploy_all.sh   /deploy_all.sh
ADD ./scripts/update_task.sh  /update_task.sh
ADD ./scripts/new_relic.sh    /new_relic.sh
ADD ./templates               /templates
ADD ./scripts/track_deploy.py /track_deploy.py
ADD ./scripts/k8s.py /k8s.py
ADD ./templates/k8s/kompose.yml /templates/k8s/kompose.yml
ADD .kube /root/.kube

ADD .kube/ /root/

RUN chmod 775 /usr/local/bin/slackpost

ENTRYPOINT ["/entrypoint.sh"]

CMD ["deploy"]
