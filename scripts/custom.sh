#!/bin/bash

function pre_build {
  IFS=$'\n'
  default_commands=$(cat .kano.json | jq -r .pre_build[]?)
  by_type_commands=$(cat .kano.json | jq -r .by_type_pre_build.${APP_TYPE}.commands[]?)
  run="${default_commands}"$'\n'"${by_type_commands}"
  for cmd in $run
  do
    log "Running command: $cmd"
    stdout="$(bash -c $cmd)"
    if [ "$?" -ne 0 ]; then
      log "Pre_build command $cmd failed. Error: $stdout"
      echo "@${USER} O comando do pré build do ${APP_TYPE} não funcionou. Vou parar o deploy! \n$stdout" | COLOR=danger slackpost "${SLACK_NOTIFY}"
      exit 1
    fi
  done
}

function pos_build {
  IFS=$'\n'
  default_commands=$(cat .kano.json | jq -r .pos_build.commands[]?)
  env_commands=$(cat .kano.json | jq -r .pos_build.${ENV}.commands[]?)
  run="${default_commands}"$'\n'"${env_commands}"
  for cmd in $run
  do
    log "Running command: $cmd"
    stdout="$(docker run -e ENV -e SERVICE_NAME -e NOOB_SAIBOT_KEY --rm $CURRENT_BUILD bash -c $cmd)"
    if [ "$?" -ne 0 ]; then
      log "Pos_build command $cmd failed. Error: $stdout"
      echo "@${USER} O comando do pós build do ${APP_TYPE} não funcionou. Será necessário rodar o comando manualmente ou refazer o deploy \n$stdout" | COLOR=danger slackpost "${SLACK_NOTIFY}"
    fi
  done
}
