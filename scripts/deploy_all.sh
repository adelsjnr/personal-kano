#!/bin/bash

source /common.sh

export AWS_DEFAULT_REGION="us-east-1"
SERVICES_LIST=$(aws ecs list-services --cluster $CLUSTER_NAME --query "serviceArns[?contains(@,'service/$REPO')]" --output text \
  | xargs aws ecs describe-services --cluster $CLUSTER_NAME --query "services[].serviceName" --services --output text \
  | sed -e "s/-$CLUSTER_NAME//g")

for service_name in $SERVICES_LIST
do
  SERVICE_NAME="$service_name"
  [[ "$SERVICE_NAME" == *"worker"* ]] && export SERVICE_TYPE=worker

  /update_task.sh $SERVICE_NAME &
done
wait

/new_relic.sh

network_prune
cd .. && rm -Rf $CLONE_PATH
