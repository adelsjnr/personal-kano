#!/bin/bash

function log {
  echo "[KANO][$CID] $*"
}

function validate_json {
  echo "JSON file: $1"

  jq -e . <<<cat $1

  if [[ "$?" -eq 0 ]]
  then
    log "Valid JSON"
    export JSON_PARSED=1
  else
    log "Invalid JSON"
    export JSON_PARSED=0
  fi
}

function replace_vars {
  FILE="$1"
  perl -pi -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : ""/eg' ${FILE}
}

function git_clone {
  log "Fetching files for ${REPO} on ${BRANCH}."
  export CLONE_PATH="${REPO}-${CID:0:4}"

  stdout="$(rm -Rf ${CLONE_PATH} ; git clone --depth 1 -b ${BRANCH} --single-branch --verbose https://${GITHUB_TOKEN}:x-oauth-basic@github.com/getninjas/${REPO}.git ${CLONE_PATH})"
  if [[ "$?" -ne 0 ]]
  then
    log "Git clone failed. Clone Path: ${CLONE_PATH} Error: $stdout"
    echo "@${USER} Falha no git clone:\n$stdout" | COLOR=danger slackpost "${SLACK_NOTIFY}"
    exit 1
  else
   cd ${CLONE_PATH} || exit
   git log -1 --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative
  fi
}

function check_semaphoreci {
  log "Check master on semaphore."

  project_id=$(curl https://api.semaphoreci.com/v2/orgs/getninjas/projects?auth_token=$SEMAPHORE_TOKEN | jq --arg REPO "$REPO" -r '.[] | select(.name==$REPO) | .id')
  branch_id=$(curl https://semaphoreci.com/api/v1/projects/$project_id/branches?auth_token=$SEMAPHORE_TOKEN | jq -r '.[] | select(.name=="master") | .id')
  master_status=$(curl https://semaphoreci.com/api/v1/projects/$project_id/$branch_id/status?auth_token=$SEMAPHORE_TOKEN | jq -r .result)

  if [[ "$master_status" == "failed" ]]
  then
    log "Build failed on semaphore"
    echo "@${USER} O build do master não passou, da uma olhada no semáforo." | COLOR=danger slackpost "${SLACK_NOTIFY}"
    exit 1
  elif [[ "$master_status" == "pending" ]]
  then
    log "Build still running"
    echo "@${USER} O build ainda ta rolando no semáforo. Espera uns minutos e tenta de novo" | COLOR=danger slackpost "${SLACK_NOTIFY}"
    exit 1
  fi
}

function docker_login {
  log "Generating docker image ${IMAGE_NAME}:${TAG_NAME} with cache."

  if [[ -n "$DOCKER_USER" && -n "$DOCKER_PASS" ]]
  then
    log "Executing docker login for ${DOCKER_USER}."
    docker login -u $DOCKER_USER -p $DOCKER_PASS
  else
    log "Executing docker login for ECR."
    $(aws ecr get-login --no-include-email)
  fi
}

function docker_repository {
  log "Creating Repo ECR ${SERVICE_NAME}"
  aws ecr create-repository --repository-name ${SERVICE_NAME}
  # aws_run ecr put-lifecycle-policy --repository-name $SERVICE_NAME --lifecycle-policy-text file://${LIFECYCLE_POLICY} # disabled until receive a fix
}

function docker_build {
  log "Building image ${IMAGE_NAME}:$TAG_NAME"
  docker build --build-arg BUNDLE_GITHUB__COM=$BUNDLE_GITHUB__COM -f ${DOCKERFILE_NAME} -t ${IMAGE_NAME} -t ${IMAGE_NAME}:$TAG_NAME .
  if [[ $? -ne 0 ]]
  then
    log "docker build failed."
    echo "@${USER} O build da imagem falhou." | COLOR=danger slackpost "${SLACK_NOTIFY}"
    exit 1
  else
    echo "@${USER} Nova imagem docker criada:\n${DOCKER_DOMAIN}/${SERVICE_NAME}:${TAG_NAME}" | slackpost "${SLACK_NOTIFY}"
  fi
}

function network_prune {
  log "Removing all unused networks"
  stdout="$(docker network prune -f)"
  if [[ $? -ne 0 ]]
  then
    log "Removing network failed:\n$stdout"
    echo "@${USER} Falha ao remover networks docker nao utilizadas:\n$stdout" | COLOR=danger slackpost "${SLACK_NOTIFY}"
    exit 1
  else
    log "Docker network unused removed!"
  fi
}

function getEnvVars() {
  echo "Loading variables from NOOB SAIBOT... from $1/$2"
  VARS_URL="https://noobsaibot.getninjas.com.br/env/$1/$2?env_type=plain"
  # shellcheck source=/dev/null
  curl -s -H "x-api-key: ${NOOB_SAIBOT_KEY}" $VARS_URL > /tmp/.env-$2
}

function loadVars() {
  # shellcheck source=/dev/null
  sed -i '1s/^/export /' /tmp/.env-$1
  # shellcheck source=/dev/null
  source /tmp/.env-$1
  # shellcheck source=/dev/null
  rm /tmp/.env-$1
}

function unsetAwsKeys() {
  if [[ -n "$AWS_ACCESS_KEY_ID" || -n "$AWS_SECRET_ACCESS_KEY" ]]
  then
    log "Unsetting AWS_KEYS"
    unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
  fi
}

function executeAndVerify() {
  if "$@"
  then
    log "$@"
  else
    log "This following command has failed: $*"
    echo "@${USER} Falha no comando $*:\n$*" | COLOR=danger slackpost "${SLACK_NOTIFY}"
    exit 1
  fi
}

function setupK8s() {
  sed -i -e "s/\${token}/${K8S_TOKEN}/" /root/.kube/config
}
