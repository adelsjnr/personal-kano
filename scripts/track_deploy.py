from snowplow_tracker import Emitter, Tracker, SelfDescribingJson
import os
import sys
import logging

logging.basicConfig()
logger = logging.getLogger(__name__)

COLLECTOR_URL = os.environ.get('SNOWPLOW_URL')
METHOD = 'post'
PROTOCOL = 'https'
BUFFER_SIZE = 1
APP_ID = 'kano'

ENVIRONMENT = os.environ.get('ENV')
SERVICE = os.environ.get('SERVICE_NAME')
DEPLOY_ID = os.environ.get('CID')
BRANCH = os.environ.get('BRANCH')
LAST_COMMIT = os.environ.get('LAST_COMMIT')
USER = os.environ.get('USER')
ACTION = sys.argv[1]

emitter = Emitter(COLLECTOR_URL, method=METHOD, protocol=PROTOCOL, buffer_size=BUFFER_SIZE)

tracker = Tracker(emitter, app_id=APP_ID, encode_base64=False)

event_payload = SelfDescribingJson(
    'iglu:br.com.getninjas/system_deploy/jsonschema/1-0-0', {
        "service": SERVICE,
        "environment": ENVIRONMENT,
        "deploy_id": DEPLOY_ID,
        "action": ACTION,
        "branch": BRANCH,
        "last_commit": LAST_COMMIT,
        "author": USER
    }
)

tracker.track_self_describing_event(event_payload)
