#!/bin/bash

set -x

source /common.sh

[[ "$SERVICE_NAME" == *"-all"* ]] && SERVICE_NAME=${SERVICE_NAME//-all/} && export DEPLOY_ALL=true

executeAndVerify getEnvVars "deploy" "kano"
executeAndVerify loadVars "kano"

executeAndVerify getEnvVars $ENV $SERVICE_NAME
executeAndVerify loadVars "$SERVICE_NAME"

exec "$@"
