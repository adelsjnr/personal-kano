#!/bin/bash

export CID="$RANDOM-$SERVICE_NAME-$ENV"

source /common.sh
source /custom.sh

log "deploying SERVICE_NAME=${SERVICE_NAME}"

if [[ -z "$SERVICE_NAME" || -z "$BRANCH" || -z "$ENV" ]]; then
  log "[KANO][$CID] You need to define a SERVICE_NAME, BRANCH and ENV"
  exit 1;
fi

export AWS_DEFAULT_REGION="us-east-1"

[[ -z "$DOCKER_DOMAIN" ]] && export DOCKER_DOMAIN=getninjas
[[ -z "$CLUSTER_NAME" ]] && export CLUSTER_NAME=${ENV}
[[ -z "$REPO" ]] && export REPO=${SERVICE_NAME}
[[ -n "$AWS_REGION" ]] && export AWS_DEFAULT_REGION=${AWS_REGION}

export SERVICE_TYPE="${SERVICE_TYPE:-worker}"
export APP_LABEL="${APP_LABEL:-$REPO.$SERVICE_TYPE.$ENV}"
export APP_TYPE="${APP_TYPE:-$(echo $APP_LABEL | cut -d. -f2)}"
export IMAGE_NAME="${DOCKER_DOMAIN}/${SERVICE_NAME}"
export DOCKERFILE_NAME="Dockerfile"

export TS=$(echo "Usando o kano. @${USER} ID do deploy: [$CID]" | slackpost "${SLACK_NOTIFY}" | jq -r .ts)

git_clone

LAST_COMMIT=$(git rev-parse --short HEAD)
export LAST_COMMIT
export TAG_NAME="${BRANCH}-${LAST_COMMIT}"
# export LIFECYCLE_POLICY="/templates/ecr-lifecycle-policy.json" # disabled until receive a fix

log "Tracking deploy on SnowPlow"
python3 /track_deploy.py start

if [[ -f ".kano.json" ]]
then
  log "Kano custom build exists. Checking json file."
  validate_json .kano.json

  if [[ "$JSON_PARSED" -eq 1 ]]
  then
    export KANO_JSON_FILE=1
  else
    log "Invalid JSON. Aborting."
    exit 1
  fi
fi

unsetAwsKeys

docker_login
docker_repository || true

if [[ "$KANO_JSON_FILE" -eq 1 ]]
then
  log "Running kano [pre_build]."
  pre_build
fi

if [[ -f "Dockerfile.${SERVICE_TYPE}" ]]
then
  log "Service Dockerfile found, using: Dockerfile.${SERVICE_TYPE}"
  export DOCKERFILE_NAME="Dockerfile.${SERVICE_TYPE}"
fi

docker pull ${IMAGE_NAME}
docker_build

executeAndVerify docker push ${IMAGE_NAME}:${TAG_NAME}

if [[ $BRANCH == master ]]
then
  executeAndVerify docker push ${IMAGE_NAME}
fi

export CURRENT_BUILD="${IMAGE_NAME}:${TAG_NAME}"

log "Preparing to deploy"
if [[ $INFRA_BACKEND_TARGET == k8s ]]
then
  log "Deploying to Kubernetes"
  python3 /track_deploy.py build-complete
  setupK8s
  /k8s.py
else
  log "Deploying to ECS"
  [[ -n "$DEPLOY_ALL" ]] && /deploy_all.sh && exit 0

  /update_task.sh $SERVICE_NAME
fi
/new_relic.sh || true

network_prune
cd .. && rm -Rf $CLONE_PATH
