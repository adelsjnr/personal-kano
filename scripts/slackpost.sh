#!/usr/bin/env bash

# Usage: slackpost "<channel>" "<message>"
# also (echo $RANDOM; echo $RANDOM) | slackpost "<channel>"

channel=$1
if [[ $channel == "" ]]
then
  echo "No channel specified"
  exit 1
fi

shift

text=$*

if [[ $text == "" ]]
then
while IFS= read -r line; do
  text="$text$line\n"
done
fi

if [[ $text == "" ]]
then
  echo "No text specified"
  exit 1
fi

escapedText=$(echo "$text" | sed 's/"/\"/g' | sed "s/'/\'/g" )

if [ -n "$COLOR" ]
then
  json="{\"as_user\": \"true\",\"user\": \"U08Q60VEE\",\"bot_id\": \"B08Q60VDY\",\"channel\": \"$channel\", \"link_names\":true, \"thread_ts\": \"$TS\",\"attachments\":[{\"color\":\"$COLOR\" , \"text\": \"$escapedText\"}]}"
else
  json="{\"as_user\": \"true\",\"user\": \"U08Q60VEE\",\"bot_id\": \"B08Q60VDY\",\"channel\": \"$channel\", \"link_names\":true, \"thread_ts\": \"$TS\",\"text\": \"$escapedText\"}"
fi

curl -s -X POST -H "Authorization: Bearer $SLACK_TOKEN" -H 'Content-type: application/json' -d "$json" https://slack.com/api/chat.postMessage
