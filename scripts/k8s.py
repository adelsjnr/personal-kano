#!/usr/bin/env python3

import os
import io
import sys
import logging
import subprocess
from subprocess import CalledProcessError
from pathlib import Path
from jinja2 import Template
from tempfile import NamedTemporaryFile, TemporaryDirectory
from ruamel.yaml import YAML

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel('DEBUG')

import sentry_sdk

SENTRY_DSN = os.environ.get('SENTRY_DSN')
logger.info('SENTRY_DSN: %s', SENTRY_DSN)
sentry_sdk.init(dsn=SENTRY_DSN)


def log_check_output(command, *args, **kwargs) -> bytes:
    logger.info('Calling: %s', command)
    return subprocess.check_output(command, *args, **kwargs)


def switch_kube_context(target):
    if target == 'production' or target == 'homolog':
        logger.info("Switching kubectl context to eks-prod")
        log_check_output('kubectl config use-context eks-prod', shell=True)
    else:
        logger.info("Switching kubectl context to eks-stage")
        log_check_output('kubectl config use-context eks-stage', shell=True)


def get_compose(template_path='/templates/k8s/kompose.yml', envs: dict = None) -> NamedTemporaryFile:
    envs = envs or {}
    with open(template_path) as template_file:
        template = Template(template_file.read())

    compose = NamedTemporaryFile(suffix='.yml', encoding='utf-8', mode='w+')
    rendered = template.render(**envs)
    logger.info(rendered)

    compose.write(rendered)
    compose.flush()
    return compose


def call_kompose_convert(compose: 'file', output_folder='./') -> str:
    os.chdir(output_folder)
    command = f'cd {output_folder} && kompose convert -v -f {compose.name}'
    response = log_check_output(command, shell=True)
    logger.info(response)
    return response


def log_file_contents(file, minlevel=logging.DEBUG) -> True:
    if logger.isEnabledFor(minlevel):  # do costly stuff only if needed
        try:
            with open(file, encoding='utf-8') as f:
                logger.debug('Contents of %s:\n%s\n', file, f.read())
            return True
        except IOError as err:
            logger.debug('Could not open %s for logging: %s', file, err)
            return False


def call_kubectl_apply(files: list, context: str) -> str:
    switch_kube_context(target=context)

    try:
        command = f'kubectl create namespace {context}'
        logger.info(log_check_output(command, shell=True))
    except CalledProcessError as err:
        if err.returncode == 1:  # namespace already exists
            logger.debug('Ignoring the error calling: %s', command)
        else:
            raise

    for file in files:
        log_file_contents(file)
        command = f'kubectl apply -f {file} --namespace {context}'
        yield log_check_output(command, shell=True)


def vendor_mutate(input_file: 'file') -> 'file':
    yaml = YAML()
    content = yaml.load(input_file)

    if content.get('kind') == 'Deployment':
        if content.mlget(['metadata', 'annotations', 'getninjas/volume']):
            hostpath, _, containerpath = content.mlget(['metadata', 'annotations', 'getninjas/volume']).partition(':')
            containerpath = containerpath or hostpath
            pathname = containerpath.replace('/', '-').strip('-')
            assert pathname

            container_name = content.mlget(['metadata', 'name'])
            assert container_name

            for i, container in enumerate(content['spec']['template']['spec']['containers']):
                if container['name'] == container_name:
                    container.setdefault('volumeMounts', []).append({
                        'name': pathname,
                        'mountPath': containerpath,
                    })
            content['spec']['template']['spec'].setdefault('volumes', []).append({
                'name': pathname,
                'hostPath': {
                    'path': hostpath
                },
            })

    output_file = io.StringIO()
    yaml.dump(content, output_file)
    output_file.seek(0)
    return output_file


def main():
    target = os.environ['CLUSTER_NAME']

    with get_compose(envs=os.environ) as compose, TemporaryDirectory() as tempfolder:
        call_kompose_convert(compose, output_folder=str(tempfolder))

        os.chdir(tempfolder)
        files = sorted(Path(tempfolder).iterdir(), key=lambda f: f.stat().st_ctime)

        # Custom mutate the converted files before applying, if needed
        for file in files:
            with open(file, mode='r+') as opened_file:
                mutated = vendor_mutate(opened_file)
                opened_file.seek(0)
                opened_file.writelines(mutated.readlines())

        for result in call_kubectl_apply(files, target):
            print(result.decode())


if __name__ == '__main__':
    logger.info('Starting k8s.py as __main__')
    logger.info('SENTRY_DSN: %s', SENTRY_DSN)
    main()
