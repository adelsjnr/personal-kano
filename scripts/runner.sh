#!/bin/bash

source /common.sh

export AWS_DEFAULT_REGION="us-east-1"
export REPO=aws-scripts
export CID="$RANDOM-runner"

git_clone && cd $SERVICE_NAME || exit

[[ -e "prepare.sh" ]] && ./prepare.sh

docker_login
docker_repository

[[ -e "build.sh" ]] && ./build.sh

echo "@${USER} Runner ${SERVICE_NAME} executado com sucesso." | slackpost "${SLACK_NOTIFY}"

network_prune
