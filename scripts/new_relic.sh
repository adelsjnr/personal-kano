#!/bin/bash

source /common.sh

function generate_post_data {
  cat <<EOF
  {
    "deployment": {
      "revision": "$TAG_NAME",
      "user": "$USER"
    }
  }
EOF
}

function registerDeployOnNewRelic {
  id=$1
  curl -X POST "https://api.newrelic.com/v2/applications/${id}/deployments.json" \
      -H "X-Api-Key:$NEW_RELIC_KEY" -i \
      -H "Content-Type: application/json" \
      -d "$(generate_post_data)"
}

function getNewRelicId {
  id=$(curl -X GET "https://api.newrelic.com/v2/applications.json" -H "X-Api-Key:$NEW_RELIC_KEY" \
    -d "filter[name]=$REPO" | jq -r ".applications[] | select(.name | match(\"$NEW_RELIC_APP_NAME\";\"i\")) | .id")
}

[[ -z "$NEW_RELIC_APP_NAME" ]] && log "I didn't find env NEW_RELIC_APP_NAME in SSM" && exit 1

getNewRelicId
[[ -z "$id" ]] && log "I didn't find any application id in new relic. See if the name is correct in SSM" && exit 1

registerDeployOnNewRelic $id
