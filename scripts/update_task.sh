#!/bin/bash

source /common.sh
source /custom.sh
SERVICE_NAME=$1

unset APP_LABEL
executeAndVerify getEnvVars $ENV $SERVICE_NAME
executeAndVerify loadVars $SERVICE_NAME

unsetAwsKeys

python3 /track_deploy.py build-complete

export APP_LABEL="${APP_LABEL:-$REPO.$SERVICE_TYPE.$ENV}"
export TASK_DEFINITION_NAME="${SERVICE_NAME}-${ENV}"

if [[ -f "task-definition-${SERVICE_NAME}.json" ]]
then
  log "Project already has task definition. Replacing with env vars."
  export TASK_DEFINITION_FILE="task-definition-${SERVICE_NAME}.json"
else
  log "Project doesn't have a task definition. Using a basic one from KANO templates."
  cp /templates/task-definition-template-${SERVICE_TYPE}.json ./$SERVICE_NAME.json
  export TASK_DEFINITION_FILE="./$SERVICE_NAME.json"
fi

executeAndVerify replace_vars ${TASK_DEFINITION_FILE}

if [[ -n "$CONTAINER_ENTRYPOINT" ]]
then
  log "Container ENTRYPOINT found, replacing default."
  TJQ=$(jq --arg arg "$CONTAINER_ENTRYPOINT" '.containerDefinitions[].entryPoint = $arg' < ${TASK_DEFINITION_FILE} | jq '.containerDefinitions[].entryPoint |= split(" ")')
  [[ $? == 0 ]] && echo "${TJQ}" >| $TASK_DEFINITION_FILE
else
  log "Container ENTRYPOINT not found."
fi

if [[ -n "$CONTAINER_CMD" ]]
then
  log "Container CMD found, replacing default."
  TJQ=$(jq --arg cmd "$CONTAINER_CMD" '.containerDefinitions[].command = $cmd' < ${TASK_DEFINITION_FILE} | jq '.containerDefinitions[].command |= split(" ")')
  [[ $? == 0 ]] && echo "${TJQ}" >| $TASK_DEFINITION_FILE
else
  log "Container CMD not found."
fi

if [[ -n "$CONTAINER_HOSTNAME" ]]
then
  log "Container HOSTNAME found, replacing default."
  TJQ=$(jq --arg arg "$CONTAINER_HOSTNAME" '.containerDefinitions[].hostname = $arg' < ${TASK_DEFINITION_FILE})
  [[ $? == 0 ]] && echo "${TJQ}" >| $TASK_DEFINITION_FILE
else
  log "Container HOSTNAME not found."
fi

python3 /track_deploy.py task-definition-created

cat ${TASK_DEFINITION_FILE}

log "Creating new task definition version of ${TASK_DEFINITION_NAME}."
aws ecs register-task-definition --cli-input-json file://${TASK_DEFINITION_FILE} > .task_result.json

if [[ $? -ne 0 ]]
then
  echo "@${USER} Não consegui registrar uma nova task-definition" | COLOR=danger slackpost "${SLACK_NOTIFY}"
  exit 1
fi

NEW_TASK=$(cat .task_result.json | jq -r '.taskDefinition.taskDefinitionArn')

log "Notify to ${SLACK_NOTIFY} the new task created ${NEW_TASK}."
echo "@${USER} Nova task definition criada\n${NEW_TASK}" | slackpost "${SLACK_NOTIFY}"

if [[ "$KANO_JSON_FILE" -eq 1 ]]
then
  log "Running kano [pos_build]."
  pos_build
fi

log "Checking service ${SERVICE_NAME}-${ENV} existence."
aws ecs list-services --cluster "${CLUSTER_NAME}" | jq '.serviceArns' | grep "${SERVICE_NAME}-${ENV}"

if [ $? -eq 0 ]
then
  log "The service ${SERVICE_NAME}-${ENV} already exists."
  log "Updating service ${SERVICE_NAME}-${ENV} on ${CLUSTER_NAME} with last version of task definition ${TASK_DEFINITION_NAME}."
  aws ecs update-service --service ${SERVICE_NAME}-${ENV} --cluster ${CLUSTER_NAME} --task-definition ${TASK_DEFINITION_NAME} > .service_result.json

  if [[ $? -ne 0 ]]
  then
    echo "@${USER} Não consegui fazer o update do service" | COLOR=danger slackpost "${SLACK_NOTIFY}"
    exit 1
  fi

  SERVICE_TASK=$(cat .service_result.json | jq -r '.service.taskDefinition')

  log "Notify service update on ${SLACK_NOTIFY}."
  echo "@${USER} Service ${SERVICE_NAME}-${ENV} atualizado com a task definition:\n${SERVICE_TASK}" | slackpost "${SLACK_NOTIFY}"

  python3 /track_deploy.py task-definition-update

  aws ecs wait services-stable --cluster $CLUSTER_NAME --services "$SERVICE_NAME-$ENV"

  if [ $? -eq 0 ]
  then
    echo "@${USER} O service ${SERVICE_NAME}-${ENV} já esta rodando com sucesso, deploy completo" | COLOR=good slackpost "${SLACK_NOTIFY}"
    python3 /track_deploy.py success
  else
    echo "@${USER} já passou 10 minutos e o service ${SERVICE_NAME}-${ENV} ainda não conseguiu subir. Olha la no painel da aws o que rolou" | COLOR=danger slackpost "${SLACK_NOTIFY}"
    python3 /track_deploy.py failed
  fi
else
  log "No service ${SERVICE_NAME}-${ENV} found. It should be created on terraform."
  python3 /track_deploy.py missing-terraform
fi
