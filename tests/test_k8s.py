import os
from unittest import mock
import pytest
import kano.k8s as k8s
import subprocess
import textwrap
import yaml

PWD = os.getcwd()


@pytest.mark.parametrize("env,context", [('goro', 'eks-stage'), ('production', 'eks-prod')])
def test_switch_kube_context(env, context):
    with mock.patch('subprocess.check_output') as subshell:
        k8s.switch_kube_context(env)
        subshell.assert_called_with(f'kubectl config use-context {context}', shell=True)


def test_get_compose():
    expected_string = '''
        version: "3"

        services:
          test:
            image: kano
            ports:
              - "300:300"
            restart: always
            environment:
              - ENV
              - SERVICE_NAME
              - NOOB_SAIBOT_KEY
            labels:
              iam.amazonaws.com/role: "test--role"
    '''

    response = k8s.get_compose(
        template_path='templates/k8s/kompose.yml',
        envs={
            'SERVICE_NAME': 'test',
            'CURRENT_BUILD': 'kano',
            'CONTAINER_PORT': 300,
        }
    )

    response.seek(0)

    assert yaml.load(expected_string) == yaml.load(response)


def test_call_kompose_convert():
    dummy_name = 'dummy'

    class Dummy:
        name = dummy_name

    with mock.patch('subprocess.check_output') as subshell:
        k8s.call_kompose_convert(compose=Dummy())
        subshell.assert_called_with(f'cd ./ && kompose convert -v -f {dummy_name}', shell=True)


def test_call_kubectl_apply():
    with mock.patch('subprocess.check_output') as subshell:
        target = 'goro'
        files = ['dummy', 'dummy2']

        list(k8s.call_kubectl_apply(files, target))

        subshell.assert_any_call(f'kubectl apply -f dummy --namespace {target}', shell=True)
        subshell.assert_called_with(f'kubectl apply -f dummy2 --namespace {target}', shell=True)


def test_mutate_file():
    input_path = f'{PWD}/tests/fixtures/test_mutate_file-output/input_test-deployment.yaml'
    input_file = open(input_path)

    output_path = f'{PWD}/tests/fixtures/test_mutate_file-output/output_test-deployment.yaml'
    output_yml = open(output_path).read()

    desired_yml = output_yml

    mutated_file = k8s.vendor_mutate(input_file)
    mutated_yml = mutated_file.read()

    assert yaml.load(mutated_yml) == yaml.load(desired_yml)
